<?php 

$mageFilename = '../app/Mage.php';
require_once $mageFilename;
//Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
umask(0);
Mage::app();
$tableName = "webslingers_form_submissions";
$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

// WIPE TABLE CLEAN
if ($_GET['wipeclean'] == 'confirmed'){
    $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
    $sqlDelete = "delete from $tableName where 1";
    $writeConnection->query($sqlDelete);
}

$sql        = "Select * from $tableName";
$rows       = $connection->fetchAll($sql); //fetchRow($sql), fetchOne($sql),...

if ($_GET['exporttocsv'] == 'true'){
    download_send_headers("data_export_" . date("Y-m-d") . ".csv");
    echo array2csv($rows);
    die();
}


function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}



?><!DOCTYPE html>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="./bbreport.js"></script>
    <link href="./bbreport.css" rel='stylesheet' type='text/css' />
    <title>Buyback Form Report</title>
</head>
<body>
<div id="wrap">
    <div class="page-title">
        <h1>Buyback Form Report</h1>
    </div>
    <div class="category-products">
    <div id="mojoNav">
        <div style="float: right;">
            <label for="searchtbl">
                <strong style="font-size: 13px;">Quick Search </strong>
            </label>
            <input type="text" id="searchtbl" name="searchtbl" style="width: 200px; height:22px;"/>
        </div>
        <button class="button btn-empty" title="Add to Cart" name="add_to_cart" type="submit" onclick="clearData(); return false;"><span><span>Purge All Data</span></span></button>
         <button class="button btn-empty" title="Clear Form" name="clear_form" type="submit" onclick="exportToCSV(); return false;"><span><span>Export to CSV</span></span></button>

    </div>
    <br />

        <div class="tables">
            <table id="tblData" style="width: 100%;">
                <thead>
                <tr>
                    <th data-sort="int" style="text-align: left; font-weight: bold; cursor: pointer; width: 20px;">ID</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 150px;">Date</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 50px;">Price</th>
                    <th data-sort="float" style="text-align: left; font-weight: bold; cursor: pointer; width: 20px;">Gen</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 50px;">Storage</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 60px;">Payment</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 140px;">Name</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 140px;">Email</th>
                    <th data-sort="string" style="text-align: left; font-weight: bold; cursor: pointer; width: 115px;">Phone</th>
                </tr>
                </thead>
                <tbody>
            <?php
                foreach ($rows as $row) {
            ?>
                    <tr data-table="table<?php echo $row["id"]?>">
                        <td style=""><?php echo $row["id"]?></td>
                        <td style=""><?php echo date('M jS Y \a\t h:i',strtotime($row["date"])); ?></td>
                        <td style="">$<?php echo $row["price"]?></td>
                        <td style=""><?php echo $row["generation"]?></td>
                        <td style=""><?php echo $row["storage"]?></td>
                        <td style=""><?php echo $row["payment_method"]?></td>
                        <td style=""><a href="#" class="showmoreinfo"><?php echo $row["name"]?></a></td>
                        <td style=""><a href="mailto:<?php echo $row["email"]?>"><?php echo $row["email"]?></a></td>
                        <td style=""><?php echo $row["phone"]?></td>
                    </tr>
            <?php
                }
            ?>
                </tbody>
            </table>
           </div>
           

            <?php
                foreach ($rows as $row) {
            ?>

            
        <div id="table<?php echo $row["id"]?>" class="moreinfo">
            <table>
                <thead>
                <tr>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">ID</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Functional</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Screen</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Buttons</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Charge</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Paypal Email</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Address</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">City</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">State</th>
                    <th data-sort="int" style="font-weight: bold; cursor: pointer;">Zip</th>
                </tr>
                </thead>
                <tbody>                        
                    <tr data-table="table<?php echo $row["id"]?>">
                        <td style=""><?php echo $row["id"]?></td>                    
                        <td style=""><?php echo $row["functional"]?></td>
                        <td style=""><?php echo $row["screen"]?></td>
                        <td style=""><?php echo $row["buttons"]?></td>
                        <td style=""><?php echo $row["charge"]?></td>
                        <td style=""><?php echo $row["paypal-email"]?></td>
                        <td style=""><?php echo $row["address"]?><br /><?php echo $row["address2"]?></td>
                        <td style=""><?php echo $row["city"]?></td>
                        <td style=""><?php echo $row["state"]?></td>
                        <td style=""><?php echo $row["zip_code"]?></td>
                    </tr>
                </tbody>
            </table>               
            <div class="instructions">[ CLICK ANYWHERE IN THIS BOX TO CLOSE IT ] </div>
        </div>
            <?php } ?>        
    </div>
</div>
</body>
</html>