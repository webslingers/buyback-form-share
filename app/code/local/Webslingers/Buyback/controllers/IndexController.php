<?php
class Webslingers_Buyback_IndexController extends Mage_Core_Controller_Front_Action
{
        //protected methods come from core/Mage/Checkout/controllers/CartController.php
    protected $_cookieCheckActions = array('add');

    public function indexAction()
    {
        $DEBUG = false;
//          $DEBUG = true;

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Buy Back your used iPods and iPhones, including iPod Touch 4 & 5, and iPhones 3G 4S 5S'));
        $this->getLayout()->getBlock('head')->setDescription($this->__('Buy Back your used iPods and iPhones, including iPod Touch 4 & 5, and iPhones 3G 4S 5S'));
        $block = $this->getLayout()->getBlock('webslingers.buyback');
        if(Mage::getSingleton('customer/session')->isLoggedIn() || is_numeric($custID)) {
            // Get customer info
            $customer_details = "[logged_in_customer_details]";
        } else {
            $customer_details = "[customer_details]";
        }
        $block->setData('customer_details',$customer_details);
        
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();
            if ($DEBUG) var_dump($post);
            
            $details = $this->validate($post);
            if ($DEBUG) var_dump($details);
 
            if (!$details['errors']){
                $buyback = Mage::getModel('webslingers_buyback/buyback');            
                $result = $buyback->getLabel($post,$details);    
                if ($result['status'] == 'success'){
                    $block->setData('success','1');
                    $buyback->recordSubmission($post);
                }
                else {
                    $block->setData('errors',array($result['errormsg']));
                    if ($DEBUG) var_dump($result);
                }
            }
            else
                $block->setData('errors',$details['errors']);

        }
        //$this->getLayout()->append($block);
        $this->renderLayout();
 
    }
   
    
    public function validate($post)
    {
    
    $details = array();
    $errors = array();
    
	if (!empty($post['generation']) && is_numeric($post['generation'])) {
		$details[] = 'Generation: '.$post['generation'].'th';
	} else {
		$errors[] = 'Please select which generation it is';
	}

	if (!empty($post['storage'])) {
		$details[] = 'Storage: '.$post['storage'];
	} else {
		$errors[] = 'Please indicate how many GB of storage it has';
	}

	if ($post['functional']==1) {
		$details[] = 'Fully Functional: YES';
	} else {
		$details[] = 'Fully Functional: NO';
	}

	if ($post['screen']!=1) {
		$details[] = 'Screen is Good: YES';
	} else {
		$details[] = 'Screen is Good: NO';
	}

	if ($post['buttons']!=1) {
		$details[] = 'Buttons are Good: YES';
	} else {
		$details[] = 'Buttons are Good: NO';
	}

	if ($post['charge']!=1) {
		$details[] = 'Charge Port is Good: YES';
	} else {
		$details[] = 'Charge Port is Good: NO';
	}

	if ($post['payment_method']!='') {
		$details[] = 'Preferred Payment Method: '.$post['payment_method'];
      	if ($post['payment_method'] == 'Paypal'){
            if ($post['paypal-email'] != '')  
                $details[] = 'Paypal Email: '.$post['paypal-email'];
            else 
                $errors[] = 'Please enter your PayPal email address';
        }
	} else {
		$errors[] = 'Please indicate how you would like to be paid';
	}

	if ($post['name']!='' && $post['name']!='Name') {
		$details[] = 'Name: '.$post['name'];
	} else {
		$errors[] = 'Please enter your name';
	}
	if ($post['email']!='' && $post['email']!='Email') {
		$details[] = 'Email: '.$post['email'];
	} else {
		$errors[] = 'Please enter your email';
	}
	if ($post['address']!='' && $post['address']!='Address') {
		$details[] = 'Address: '.$post['address'];
	} else {
		$errors[] = 'Please enter your address';
	}
	if ($post['address2']!='' && $post['address2']!='Address Line 2') {
		$details[] = ''.$post['address2'];
	} else {
	}
	if ($post['city']!='' && $post['city']!='City') {
		$details[] = 'City: '.$post['city'];
	} else {
		$errors[] = 'Please enter your city';
	}
	if ($post['state']!='' && $post['state']!='State') {
		$details[] = 'State: '.$post['state'];
	} else {
		$errors[] = 'Please enter your state';
	}
	if ($post['zip_code']!='' && $post['zip_code']!='Zip Code') {
		$details[] = 'Zip Code: '.$post['zip_code'];
	} else {
		$errors[] = 'Please enter your zip code';
	}
	if ($post['phone']!='' && $post['phone']!='Phone') {
		$details[] = 'Phone: '.$post['phone'];
	} else {
		$errors[] = 'Please enter your phone';
	}

	$generation    = $post['generation'];
	$storage       = $post['storage'];
	$functional    = $post['functional'];
	$crackedscreen = $post['screen'];
	$buttons       = $post['buttons'];
	$charge        = $post['charge'];
	$headphone     = $post['headphone'];
	$your_price    = 0;

	if ($generation=="4") { /* iPod 4 */
		
		if ($functional=="1") {
			$your_price = 41;
		}

		if ($crackedscreen=="1") {
			$your_price = 28; /* Cracked Screen */
		}

		if ($buttons=="1" && $crackedscreen!="1") {
			$your_price = 33; /* Button issues but screen is OK */
		} else if ($buttons=="1" && $crackedscreen=="1") {
			$your_price = 20; /* Button issues & screen cracked */
		}

		if ($charge=="1" && $crackedscreen!="1") {
			$your_price = 37; /* Charge port bad, but screen is OK */
		} else if ($charge=="1" && $crackedscreen=="1") {
			$your_price = 10; /* Charge port bad & screen is cracked */
		} else if ($your_price > 0) { /* $10 more for each larger GB size, IF charge port is OK */
			if ($storage=="16GB") {
				$your_price = $your_price+10;
			} else if ($storage=="32GB") {
				$your_price = $your_price+20;
			} else if ($storage=="64GB") {
				$your_price = $your_price+30;
			}
		}
	}
	if ($generation=="5") { /* iPod 5 */

		if ($functional=="1") {
			$your_price = 95;
		}

		if ($crackedscreen=="1") {
			$your_price = 70; /* Cracked Screen */
		}

		if ($buttons=="1" && $crackedscreen!="1") {
			$your_price = 75; /* Button issues but screen is OK */
		} else if ($buttons=="1" && $crackedscreen=="1") {
			$your_price = 60; /* Button issues & screen cracked */
		}

		if ($charge=="1" && $crackedscreen!="1") {
			$your_price = 40; /* Charge port bad, but screen is OK */
		} else if ($charge=="1" && $crackedscreen=="1") {
			$your_price = 30; /* Charge port bad & screen is cracked */
		} else if ($your_price > 0) { /* $10 more for each larger GB size, IF charge port is OK */
			if ($storage=="32GB") {
				$your_price = $your_price+10;
			} else if ($storage=="64GB") {
				$your_price = $your_price+20;
			}
		}
	}

    $details[] = 'Quoted Price: '.$your_price;
    
    if (empty($errors))
        $details['errors'] = false;
    else $details['errors'] = $errors;
   
    return $details;
    


    }
}
