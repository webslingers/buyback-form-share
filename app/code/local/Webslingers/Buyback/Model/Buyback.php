
<?php
class Webslingers_Buyback_Model_Buyback extends Mage_Core_Model_Abstract
{

    private $DEBUG = false;
    
    protected function _construct()
    {
         $this->_init('webslingers_buyback/buyback');
         if (isset($_GET['debug']))
            $this->$DEBUG = true;
    }
          
    
    public function getLabel($info, $details) 
    {
        
        require('app/code/local/Webslingers/Buyback/lib/autoload.php');
        require('app/code/local/Webslingers/Buyback/lib/fpdf.php');
        
        if ($this->$DEBUG) echo "about to inst";
        
        $shipment = new \RocketShipIt\Shipment('UPS');
        $validate = new \RocketShipIt\AddressValidate('UPS');
        $package = new \RocketShipIt\Package('UPS'); 
        
        // SHIPMENT INFO
        
        $shipment->setParameter('shipContact', $info["name"]);
        $shipment->setParameter('shipper', $info["name"]);
        $shipment->setParameter('shipAddr1', $info["address"]);
        $shipment->setParameter('shipCity', $info["city"]);
        $shipment->setParameter('shipState', $info["state"]);
        $shipment->setParameter('shipCode',  $info["zip_code"]);          
        $shipment->setParameter('shipAddr2', $info["address2"]);
        $shipment->setParameter('shipPhone', $info["phone"]);
  /*
          $validate->setParameter('toAddr1',$info["address"]);
          $validate->setParameter('toCity',$info["city"]);
          $validate->setParameter('toState',$info["state"]);
          $validate->setParameter('toCode',  $info["zip_code"]);
          $validation = $validateStreetLevel();
    echo "validated address";
          var_dump($validation);
          die();
*/    

        $shipment->setParameter('toCompany', 'Repair \'Em');
        $shipment->setParameter('toPhone', '909-255-10615');
        $shipment->setParameter('toAddr1', '420 E Hospitality Lane');
        $shipment->setParameter('toAddr2', 'Suite A-2');
        $shipment->setParameter('toCity', 'San Bernadino');
        $shipment->setParameter('toState', 'CA');
        $shipment->setParameter('toCode', '92408');

        
        // PACKAGE INFO 
        $package->setParameter('shipContact', $info["name"]);
        $package->setParameter('shipAddr1',$info["address"]);
        $package->setParameter('shipAddr2', $info["address2"]);
        $package->setParameter('shipCity',$info["city"]);
        $package->setParameter('shipState',$info["state"]);
        $package->setParameter('shipCode',  $info["zip_code"]);
        $package->setParameter('shipPhone',$info["phone"]); 
        $package->setParameter('length','5');
        $package->setParameter('width','5');
        $package->setParameter('height','5');
        $package->setParameter('weight','1');
        $shipment->addPackageToShipment($package);

        // SUBMIT INFO
        if ($this->$DEBUG) var_dump($shipment->getData());
        $label = $shipment->submitShipment();
        if ($this->$DEBUG) var_dump($label);

        if (is_string($label))
            return (array('result'=>'failure','errormsg'=>$label));
            
        $labelhash = md5(base64_decode($label['pkgs'][0]['label_img']));
        $labelgif = imagecreatefromstring(base64_decode($label['pkgs'][0]['label_img']));
        $rotatedgif = imagerotate($labelgif, 270, 0);


        //  create file
        $site_url = 'http://repairem.com';
        $savedir = "/media/return_labels/";
        $savepath = getcwd() . $savedir;
        $labelfile = $savepath . "$labelhash.gif";
        $pdffile = $savepath . "$labelhash.pdf";
        //echo $labelfile;
        imagegif($rotatedgif, $labelfile);
        //imagegif($finalgif, $labelfile);
        //$f = fopen($labelfile,'w');
        //fwrite($f, );
        //fclose($f);

        //header('Contents-Type: image/gif');
        //echo base64_decode($label['pkgs'][0]['label_img']);

        
        $pdf = new FPDF();
        $pdf->AddPage();

        $pdf->Image($site_url . $savedir .  "$labelhash.gif", 10,10,120);
        $pdf->Output($pdffile, 'F');
        $pdflink = $site_url . $savedir . "$labelhash.pdf";
        
        
        
        // SEND EMAIL  #todo: create a separate method for this
        //
        
        $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('return_label');
        if ($this->$DEBUG) var_dump($emailTemplate);
        
        $postinfo = '';      
        
        // DETAILS
        $postinfo .= '<br><b>Details</b><br>';
        foreach ($details as $deet)
            $postinfo .= "$deet <br>";

        // POST INFO
        $postinfo .= '<br><b>Post info</b><br>';
        foreach ($info as $var => $val)
            $postinfo .= $var .'  -  '. $val . '<br>';

            
        $emailvars['postinfo'] = $postinfo;
        $emailvars['labelhash'] = $pdflink;

        $processedTemplate = $emailTemplate->getProcessedTemplate($emailvars);
        
        //Getting the Store E-Mail Sender Name.
        $senderName = 'Repair \'Em Buyback';

        //Getting the Store General E-Mail.
        //$senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $senderEmail = 'obscured@repairem.net';

    
        //Sending E-Mail to Customers.
        $mail = Mage::getModel('core/email')
            ->setToName($info['name'])
            ->setToEmail($info['email'])
            ->setBody($processedTemplate)
            ->setSubject('REPAIR \'EM BUYBACK: Your shipping label and packing list')  
            ->setFromEmail($senderEmail)
            ->setFromName($senderName)
            ->setType('html');
        //Sending E-Mail to Customers.
        $mail2 = Mage::getModel('core/email')
            ->setToName($info['name'])
            ->setToEmail('obscured@repairem.com')
            ->setBcc(array(''))
            ->setBody($processedTemplate)
            ->setSubject('REPAIR \'EM BUYBACK: Your shipping label and packing list')  
            ->setFromEmail($senderEmail)
            ->setFromName($senderName)
            ->setType('html');
        try{
        //Confimation E-Mail Send
            $mail->send();
            $mail2->send();
        }
        catch(Exception $error)
        {
            Mage::getSingleton('core/session')->addError($error->getMessage());
            return false;
        }

        return array('status'=>'success');
        //$pdf->Output('UPS Label','I');

//            echo "<a href='http://repairem.com" . $savedir .  "$labelhash.pdf" . "' > file </a>";
          

    }
    
    public function recordSubmission($data)
    {
    
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $tableName = "webslingers_form_submissions";
        
        $sql = "INSERT INTO `$tableName` 
(`id`, `price`, `generation`, `storage`, `functional`, `screen`, `buttons`, `charge`, `payment_method`, `paypal-email`, `name`, `email`, `address`, `address2`, `city`, `state`, `zip_code`, `phone`) 
VALUES 
(NULL, 
 '".$data['price']."',
 '".$data['generation']."',
 '".$data['storage']."',
 '".$data['functional']."',
 '".$data['screen']."',
 '".$data['buttons']."',
 '".$data['charge']."',
 '".$data['payment_method']."',
 '".$data['paypal-email']."',
 '".$data['name']."',
 '".$data['email']."',
 '".$data['address']."',
 '".$data['address2']."',
 '".$data['city']."',
 '".$data['state']."',
 '".$data['zip_code']."',
 '".$data['phone']."')";
     
       $writeConnection->query($sql);
    }
    
}
