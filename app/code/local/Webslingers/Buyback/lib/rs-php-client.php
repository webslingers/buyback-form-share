<?php
/**
 * Copyright RocketShipIt LLC All Rights Reserved
 * Author: Mark Sanborn
 * Version: 1.1.3.5
 * PHP Version 5
 * For Support email: support@rocketship.it
**/

/*

Heads Up! The demo is a wrapper that passes through RocketShipIt servers and does not communicate directly to the carrier's servers. Once you purchase RocketShipIt you will be given the entire unobfuscated script which will talk directly to the carriers.

* The demo passes through RocketShipIt servers, the real RocketShipIt module communicates directly with the carriers
* You will only be able to operate in development mode and produce sample labels
* To produce live labels you will need to purchase the full RocketShipIt module
* Only UPS/FedEx and USPS are available in the demo, Stamps.com is not included
* Almost all RocketShipIt features are available but some are unavailable i.e. Pickup Requests
* International labels are limited in the demo version
* Rates produced from the RocketShipIt demo will be for evaluation purposes only and not always be accurate

Remember to fill in the config.php file with your FedEx/UPS/USPS credentials!

*/

namespace RocketShipIt;

// RocketShipIt Config
require('config-demo.php');

/**
* Ensures that only settable paramaters are allowed.
*
* This function aids the setPramater() function in that it only
* allows known paramaters to be set.  This helps to avoid typos when
* setting parameters.
*/
function rocketshipit_getOKparams($carrier) {
    // Force fedex, FedEx, FEDEX to all read the same
    $carrier = strtoupper($carrier);

    // Generic parameters that are accessible in each class regardless of carrier
    $generic = array('shipper','enableRocketShipAPI','shipContact','shipPhone',
                     'accountNumber','shipAddr1','shipAddr2','shipAddr3',
                     'shipCity','shipState','shipCode','shipCountry',
                     'toCompany','toName','toPhone','toAddr1','toAddr2',
                     'toAddr3','toCity','toState','toCountry','toCode',
                     'service','weightUnit','length','width','height',
                     'weight','toExtendedCode', 'currency', 'toAttentionName',
                     'fromName', 'fromAddr1', 'fromAddr2', 'fromCity', 'fromState',
                     'fromCode', 'fromExtendedCode');

    // Carrier specific parameters
    switch ($carrier) {
        case "UPS":
            $specific = array('earliestTimeReady','latestTimeReady',
                              'httpUserAgent','labelPrintMethodCode',
                              'labelDescription','labelHeight','labelWidth',
                              'labelImageFormat','residentialAddressIndicator',
                              'PickupType', 'pickupDescription',
                              'shipmentDescription','packagingType',
                              'packageLength','packageWidth','packageHeight',
                              'packageWeight','referenceCode','referenceValue',
                              'insuredCurrency','monetaryValue',
                              'referenceCode2','referenceValue2','pickupDate',
                              'lengthUnit','serviceDescription','returnCode',
                              'fromAddr2','fromAddr3','fromCountry',
                              'fromAttentionName','fromPhoneNumber','fromFaxNumber',
                              'packageDescription','returnEmailAddress',
                              'returnUndeliverableEmailAddress',
                              'returnFromEmailAddress','returnEmailFromName',
                              'verifyAddress','negotiatedRates',
                              'saturdayDelivery','billThirdParty',
                              'thirdPartyAccount','codFundType',
                              'codAmount','flexibleAccess', 'signatureType',
                              'customerClassification', 'pickupAddr1',
                              'pickupCity', 'pickupState', 'pickupCode',
                              'pickupCountry', 'pickupResidential', 'pickupAlternative',
                              'closeTime', 'readyTime', 'pickupCompanyName', 'pickupContactName',
                              'pickupPhone','pickupServiceCode', 'pickupQuantity', 'pickupDestination',
                              'pickupContainerCode', 'pickupAlternative', 'pickupOverweight',
                              'paymentMethodCode', 'pickupCardHolder', 'pickupCardType', 
                              'pickupCardNumber', 'pickupCardExpiry', 'pickupCardSecurity',
                              'pickupCardAddress', 'pickupCardCountry', 'pickupPRN', 'trackingNumber',
                              'pickupRoom', 'pickupFloor', 'thirdPartyPostalCode',
                              'thirdPartyCountryCode','invoiceLineNumber', 'invoiceLineDescription',
							  'invoiceLineValue', 'invoiceLinePartNumber', 'invoiceLineOriginCountryCode',
							  'invoice', 'invoiceDate', 'invoiceReason', 'invoiceCurr', 'additionalDocs',
							  'soldName', 'soldCompany', 'soldTaxId', 'soldPhone', 'soldAddr1', 'soldAddr2',
							  'soldCity', 'soldState', 'soldCode', 'soldCountry',
                              'license', 'username', 'password');
            break;
        case "USPS":
            $specific = array('userid','imageType','weightPounds',
                              'weightOunces','firstClassMailType',
                              'packagingType','pickupDate', 'permitNumber',
							  'permitIssuingPOCity', 'permitIssuingPOState',
							  'permitIssuingPOZip5', 'pduFirmName', 'pduPOBox',
							  'pduCity', 'pduState', 'pduZip5', 'pduZip4',
                              'returnEmailAddress', 'returnFromName',
                              'returnFromEmailAddress','returnEmailFromName',
                              'returnToName', 'referenceValue');
            break;
        case "FEDEX":
            $specific = array('key','packagingType','weightUnit','lengthUnit',
                              'dropoffType','residential','paymentType',
                              'labelFormatType','imageType','labelStockType',
                              'packageCount','sequenceNumber','trackingIdType',
                              'trackingNumber','shipmentIdentification',
                              'pickupDate','signatureType','referenceCode',
                              'referenceValue', 'smartPostIndicia', 'smartPostHubId',
                              'smartPostEndorsement', 'smartPostSpecialServices',
                              'insuredCurrency', 'insuredValue', 'saturdayDelivery',
                              'residentialAddressIndicator', 'customsDocumentContent',
                              'customsValue', 'customsNumberOfPieces',
                              'countryOfManufacture', 'customsWeight',
                              'customsCurrency', 'collectOnDelivery', 'codCollectionType', 
                              'codCollectionAmount', 'holdAtLocation', 'holdPhone', 'holdStreet',
                              'holdCity', 'holdState', 'holdCode', 'holdCountry', 'holdResidential',
                              'saturdayDelivery', 'futureDay', 'shipDate', 'nearPhone',
                              'nearCode', 'nearAddr1', 'nearCity', 'nearState', 'returnCode',
                              'referenceValue2', 'referenceCode2', 'referenceValue3', 'referenceCode3',
                              'emailMessage', 'emailRecipientType', 'emailTo', 'emailFormat',
                              'emailLanguage', 'thirdPartyAccount', 'key', 'password', 'meterNumber');
            break;
        case "STAMPS":
            $specific = array('weightPounds', 'imageType', 'packagingType',
                              'declaredValue', 'customsContentType', 'customsComments',
                              'customsLicenseNumber', 'customsCertificateNumber',
                              'customsInvoiceNumber', 'customsOtherDescribe',
                              'customsDescription', 'customsQuantity', 'customsValue',
                              'customsWeight', 'customsHsTariff', 'customsOriginCountry',
                              'insuredValue', 'referenceValue', 'weightOunces');
            break;
        default: 
            throw new \RuntimeException("Invalid carrier '$carrier' in getOKparams");
    }
    return array_merge($generic, $specific);
}

/**
* Gets defaults
*
* This function will grab defaults from config.php
*/
function rocketshipit_getParameter($param, $value, $carrier) {
    // Force fedex, FedEx, FEDEX to all read the same
    $carrier = strtoupper($carrier);

    // If the default is not in the getOKparams function an exception is thrown
    if (!in_array($param, rocketshipit_getOKparams($carrier)) && $param != '') {
        throw new \RuntimeException("Invalid parameter '$param' in setParameter");
    }

    if ($value == "") { // get the default, if set
        $value = getGenericDefault($param);
        if ($value == "") { // not in the generics? look in the specific carrier params
            switch ($carrier) {
                case "UPS":
                    $value = getUPSDefault($param);
                    break;
                case "USPS":
                    $value = getUSPSDefault($param);
                    break;
                case "FEDEX":
                    $value = getFEDEXDefault($param);
                    break;
                case "STAMPS":
                    $value = getSTAMPSDefault($param);
                    break;
                default:
                    throw new \RuntimeException("Unknown carrier in setParameter: '$carrier'");
            }
        }
    }
    return $value;
}

class RocketShipGeneric
{
    public $debugInfo;

    function __construct($carrier)
    {
        $this->carrier = strtoupper($carrier);
        $this->OKparams = rocketshipit_getOKparams($carrier);
        $this->parameters = array();
        $this->packages = array();
        $this->core = new RocketShipCore;

        foreach ($this->OKparams as $param) {
            $this->setParameter($param, '');
        }
    }

    function addPackageToShipment($packageObj) {
        $params = array();
        foreach ($packageObj->parameters as $param => $value) {

            if ($value == '') {
                continue;
            }

            if ($this->parameters[$param] != $value) {
                $params[$param] = $value;
            }
        }
        $this->packages[] = $params;
    }

    function addCustomsLineToShipment($customsObj) {
        $this->customs[] = $customsObj->parameters;
    }

    function setParameter($key, $value) {
        $value = rocketshipit_getParameter($key, $value, $this->carrier);
        $this->parameters[$key] = $value;
    }

    function request($data) {
        $url = 'http://demo.rocketship.it';
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $curlReturned = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($curlReturned, true);
        if (in_array('debug', array_keys($response))) {
            $this->debugInfo = $response['debug'];
        }
        return $response;
    }

    function getData() {
        $data = array();
        $data['debug'] = true;
        $data['carrier'] = $this->carrier;
        $data['parameters'] = $this->parameters;
        if (!empty($this->packages)) {
            $data['parameters']['packages'] = $this->packages;
        }
        if (!empty($this->customs)) {
            $data['parameters']['customs'] = $this->customs;
        }
        return $data;
    }

    public function debug()
    {
        return html_entity_decode($this->debugInfo);
    }
}

class RocketShipCore
{
    function debug() {
        return $this->debugInfo; 
    }
}

class Void extends RocketShipGeneric
{
    public $type = 'Void';

    function voidShipment() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }
}

class Rate extends RocketShipGeneric
{
    public $type = 'Rate';
    
    function getAllRates() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }

    function getSimpleRates() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }

    function getRate() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }
}

class TimeInTransit extends RocketShipGeneric
{
    public $type = 'TimeInTransit';

    function getUPSTimeInTransit() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }

    function getTimeInTransit() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }
}

class AddressValidate extends RocketShipGeneric
{
    public $type = 'AddressValidate';

    function validate() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }

    function validateStreetLevel() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }
}

class Track extends RocketShipGeneric
{
    public $type = 'Track';

    function track($track_num) {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }

    function trackByReference($reference) {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $args = func_num_args();
        if (func_num_args() > 0) {
            $data['args'] = func_get_args();
        }
        $response = $this->request($data);
        return $response['carrier_response'];
    }
}

class Shipment extends RocketShipGeneric
{
    public $type = 'Shipment';

    function submitShipment() {
        $data = $this->getData();
        $data['type'] = $this->type;
        $data['action'] = __FUNCTION__;
        $data['debug'] = true;
        $response = $this->request($data);
        return $response['carrier_response'];
    }

}

class Package
{
    function __construct($carrier)
    {
        $this->carrier = strtoupper($carrier);
        $this->parameters = array();
        $this->carrier = strtoupper($carrier);
        $this->OKparams = rocketshipit_getOKparams($carrier);
        //var_dump($this->OKparams);
        
        foreach ($this->OKparams as $param) {
            $this->setParameter($param, '');
        }
    }

    function setParameter($key, $value)
    {
        $value = rocketshipit_getParameter($key, $value, $this->carrier);
        $this->parameters[$key] = $value;
    }

}

class Customs
{
    function __construct($carrier)
    {
        $this->carrier = strtoupper($carrier);
        $this->parameters = array();
        $this->carrier = strtoupper($carrier);
        $this->OKparams = rocketshipit_getOKparams($carrier);
        foreach ($this->OKparams as $param) {
            $this->setParameter($param, '');
        }
    }

    function setParameter($key, $value)
    {
        $value = rocketshipit_getParameter($key, $value, $this->carrier);
        $this->parameters[$key] = $value;
    }
}
